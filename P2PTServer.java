/**
 * @author Ashwin Ramesh aram7972 - 311254012
 * This class deals with collecting all "tweets" from the other peers and storing it in the hash map 
 * Compile all files in the folder.
 * run "java P2PTwitter <user-name>"
 */


import java.net.*;

public class P2PTServer extends Thread{
	
	public P2PTServer() {
		
	}
	
	public void run() {
		while(true){
			byte[] buf = new byte[160]; // 140 message + 19 for unikey + 1 for colon
			DatagramPacket dgp = new DatagramPacket(buf, buf.length);
			try {
				P2PTwitter.socket.receive(dgp);
				String message = new String(dgp.getData(),0,dgp.getLength(),P2PTwitter.encodingFormat); // String(byte[] bytes, int offset, int length, Charset charset) 
				String tempMessage[] = message.split(":");
				P2PTwitter.messageHashMap.replace(tempMessage[0], tempMessage[1]);				
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
