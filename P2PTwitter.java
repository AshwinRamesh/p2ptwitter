/**
 * @author Ashwin Ramesh aram7972 - 311254012
 * This class is the main class. 
 * Compile all files in the folder.
 * run "java P2PTwitter <user-name>"
 */


import java.io.*;
import java.net.*;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class P2PTwitter {
	
	static final String encodingFormat = "ISO-8859-1";
	static final int PORT = 7014;
	static final String defaultMessage = "<Not Initialised>";
	
	static DatagramSocket socket;
	static String unikey;
	static String userPseudo = null;
	static int numberOfPeers;
	static Properties userProperties;
	static ConcurrentHashMap<String, String> messageHashMap;
	public static void main(String[] args){
		
		/* Check for Username Argument "java P2PTwitter <username>"*/
		if (args.length != 1) {
			System.out.println("Error! Please provide your unikey. E.g. java P2PTwitter <unikey here>");
			//unikey = "aram7972";
			System.exit(1);
		}
		unikey = args[0];
		
		/* Load Properties */
		try {
			userProperties = new Properties();
			userProperties.load(new FileInputStream("participants.properties"));
		}
		catch(IOException e){
			System.out.println("Error! Cannot parse properties file. Please check formatting!");
			System.exit(1);
		}
		
		/* Check if user exists in properties */
		numberOfPeers = userProperties.getProperty("participants").split(",").length;
		//System.out.println(numberOfPeers);
		for (int i = 0; i < numberOfPeers; i++) {
			//System.out.println(i + "<-- is i");
			if(userProperties.getProperty("peer"+(i+1)+".unikey").equals(unikey)){
				userPseudo = userProperties.getProperty("peer"+(i+1)+".pseudo");
				break;
			}
		}
		if (userPseudo == null) {
			System.out.println("Error! Your unikey does not exist in the properties file. Please add it then run P2PTwitter.");
			System.exit(1);
		}
		
		/* Hashmap to handle concurrent read and writes */
		messageHashMap = new ConcurrentHashMap<String, String>();
		for(int i = 0; i < numberOfPeers; ++i){
			String key = userProperties.getProperty("peer" + (i+1) + ".unikey");
			messageHashMap.put(key, defaultMessage);
		}
		
		/* Initialise Socket */
		try{
			socket = new DatagramSocket(PORT);
		}
		catch(SocketException e) {
			e.printStackTrace();
		}
		
		/* Start P2p2Twitter */
		new P2PTClient().start();
		new P2PTServer().start();
		new Communication().start();
		
	}
	

}
