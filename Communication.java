/**
 * @author Ashwin Ramesh aram7972 - 311254012
 * This class is to send the user's data to the other peers
 * Compile all files in the folder.
 * run "java P2PTwitter <user-name>"
 */

import java.net.*;
import java.util.*;

public class Communication extends Thread{
	
	public Communication(){
		
	}

	public void run(){
		Random randomGenerator = new Random();
		while(true){
			String userStatus = P2PTwitter.messageHashMap.get(P2PTwitter.unikey);
			try {
				Thread.sleep(1000 + randomGenerator.nextInt(2000)); // between 1000-3000 ms = 1-3 sec
				
			}
			catch(Exception e){
				e.printStackTrace();
			}
			for (int i = 0; i < P2PTwitter.numberOfPeers; i++) {
				if(P2PTwitter.userProperties.get("peer"+(i+1)+".unikey").equals(P2PTwitter.unikey)) {
					continue;
				}
				try {
					InetAddress IPAddress = InetAddress.getByName(P2PTwitter.userProperties.getProperty("peer" + i + ".ip"));
					byte[] buf = (P2PTwitter.unikey + ":" + userStatus).getBytes(P2PTwitter.encodingFormat);
					P2PTwitter.socket.send(new DatagramPacket(buf, buf.length, IPAddress, P2PTwitter.PORT));
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
