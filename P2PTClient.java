/**
 * @author Ashwin Ramesh aram7972 - 311254012
 * This class is to get the user's data and print other peers data 
 * Compile all files in the folder.
 * run "java P2PTwitter <user-name>"
 */


import java.io.*;

public class P2PTClient extends Thread{
	
	public P2PTClient(){
		
	}

	public void run() {
		while(true){
			
			/* Get User Data */
			String status = null;
			System.out.print("Status: ");
			BufferedReader statusInput = new BufferedReader(new InputStreamReader(System.in));
			
			try {
				status = statusInput.readLine();
				if(status.length() > 140){
					System.out.println("Error! Status is over the 140 character limit!");
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
			P2PTwitter.messageHashMap.replace(P2PTwitter.unikey,status);
			
			/* Print Peer Data */

			System.out.println("### P2P tweets ###");
			for (int i = 0; i < P2PTwitter.numberOfPeers; i++) {
				String tempUnikey = (String) P2PTwitter.userProperties.get("peer"+(i+1)+".unikey");
				String tempStatus = P2PTwitter.messageHashMap.get(tempUnikey);
				if (tempUnikey.equals(P2PTwitter.unikey)) {
					System.out.println("# "+ P2PTwitter.userProperties.getProperty("peer" + (i+1) + ".pseudo") + " (myself) : " + tempStatus);
				}
				else {
					System.out.println("# "+ P2PTwitter.userProperties.getProperty("peer" + (i+1) + ".pseudo") + " (" + tempUnikey + ") : " + tempStatus);
				}
			}
			System.out.println("### End tweets ###");
		}
	}

}
